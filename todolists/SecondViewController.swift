//
//  SecondViewController.swift
//  todolists
//
//  Created by Thanisorn Jundee on 2/1/2560 BE.
//  Copyright © 2560 Thanisorn Jundee. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var inputText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func Sender(_ sender: Any) {
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let text = inputText.text
        if segue.identifier == "bReturn"
        {
            var m:[String] = UserDefaults.standard.object(forKey: "ListD") as! [String]
            m.append(text!)
            UserDefaults.standard.set(m, forKey: "ListD")
            print(UserDefaults.standard.object(forKey: "ListD")!)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

