//
//  FirstViewController.swift
//  todolists
//
//  Created by Thanisorn Jundee on 2/1/2560 BE.
//  Copyright © 2560 Thanisorn Jundee. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var resultTable: UITableView!
    var rray:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rray = UserDefaults.standard.object(forKey: "ListD") as! [String]
    }
    override func viewDidAppear(_ animated: Bool) {
        rray = UserDefaults.standard.object(forKey: "ListD") as! [String]
        resultTable.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = rray[indexPath.row]
        cell.detailTextLabel?.text = ""
        return cell
    }
}

